package jp.alhinc.iwata_kazuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class CalculateSales {

	public static void main(String[] args) {
		//支店定義ファイルと出力ファイルのマップと保持。
		Map<String, String> branchMap = new HashMap<>();
		//売り上げマップと出力ファイルのマップと保持。
		Map<String,Long> salesMap = new HashMap<>();
		//ファイルを読み込むためのクラス
		BufferedReader br = null;
		try {
			File files = new File(args[0], "branch.lst");
			//フォルダの存在確認
	        if (files.exists()) {
				br = new BufferedReader(new FileReader(files));
				String line;
				while ((line = br.readLine()) != null) {
					//bankの中身を区切って入れる。。
					String[] bank = line.split(",");
					if(2!= bank.length) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
					}
					branchMap.put(bank[0], bank[1]);
					salesMap.put(bank[0],0L);
				}
			} else {
	            System.out.println("支店定義ファイルが存在しません");
	            return;//処理を終了
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if(br!= null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		// salesFileのリスト作成
		ArrayList<String> salesFile = new ArrayList<String>();
		File dir = new File(args[0]);
		//allFileの一覧を取得する。
		File[] allFile = dir.listFiles();
		for (int i = 0;  i < allFile.length;i++) {
			if (allFile[i].getName().matches("^[0-9]{8}.rcd$")) {
				//ファイルを取得し、リストに入れる。
				salesFile.add(allFile[i].getName());
				//ループを二つに分ける
			//集計エラー1連番になっていない場合
				//0から8番目までの数字を抜き取る
				String fileNumber = salesFile.get(i).substring(0,8);
				String nextFileNumber = salesFile.get(i++).substring(0,8);
				int fileNumberInt = Integer.parseInt(fileNumber);
				int nextFileNumberInt =  Integer.parseInt( nextFileNumber);
				if(fileNumberInt +1 != nextFileNumberInt) {
					System.out.println("売り上げファイル名が連番になっていません");
				 return;
				}
			}
		}
		//売り上げファイル開いて処理を行う
		BufferedReader br1 = null;
		try {
			for (int i = 0; i < salesFile.size() ;i++ ) {
				File file = new File(args[0], salesFile.get(i));
				br1 = new BufferedReader(new FileReader(file));
				ArrayList<String> saleBox = new ArrayList<String>();
				int countLine=0;
				//saleの中身を一行ずつ読み込む
				String sale;
				while ((sale = br1.readLine()) != null) {
					//売り上げファイルの金額に数字以外正規表現でif文で判定させる
					if(sale.matches("[0-9]{8a}"))
					System.out.println("予期せぬエラーが発生しました");
					//エラー4
					countLine++;
					if(countLine==3) {
						System.out.println(file.getName() +"のフォーマットが不正です");
						return;
					}
					//リストにsaleの値を入れる。
					saleBox.add(sale);
				}
				//エラー２
				//支店コード取得
				String branchCode = saleBox.get(0);
				//ロング型に変換して小計を取得
				long salesSubTotal = Long.parseLong(saleBox.get(1));
				//売り上げマップからキーを元に売り上げ合計を取得
				long salesTotal = salesMap.get(branchCode);
				if ( salesSubTotal + salesTotal>=10000000000L) {
					System.out.println("合計金額が10桁を超えました");
				}
		        long lon = Long.parseLong(saleBox.get(1));
		        salesMap.put(saleBox.get(0),lon + salesMap.get(saleBox.get(0)));
				br1.close();
				//集計エラー3支店に該当がない場合
				if( !branchMap.containsKey( branchCode )) {
					System.out.println(file.getName() + "の支店コードが不正です");
					br1.close();
				return;
				}
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(br1!= null) {
				try {
					br1.close();
				}catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		//出力用ファイル作成する
		try{
			File file = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(file);
			PrintWriter pw = new PrintWriter(new BufferedWriter(fw));
			for (String key : branchMap.keySet()) {
				//System.out.println(key + "," + branchMap.get(key) + "," + salesMap.get(key));
				pw.println(key + "," + branchMap.get(key) + "," + salesMap.get(key));
			}
		    pw.close();
		}catch(IOException e){
		    System.out.println("予期せぬエラーが発生しました");
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			 return;
		} finally {
		}
	}
}
